/**
 * Created by heliz on 1/28/2020 AD.
 */
function myFunction() {
  var dots = document.getElementById("dots");
  var moreText = document.getElementById("more");
  var btnText = document.getElementById("myBtn");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "توضیحات بیشتر";
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "توضیحات کمتر";
    moreText.style.display = "inline";
  }
}