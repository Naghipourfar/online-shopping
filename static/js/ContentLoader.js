function loadContent() {
    var contentTag = $("#content");
    var content = `<style type="text/css">
                    body {
                        background: #eff6fa;
                    }

                    #sidebar {
                        position: fixed;
                        top: 51.8px;
                        right: 0;
                        bottom: 0;
                        width: 18%;
                        background-color: #ffffff;
                        padding: 0px;
                    }

                    #sidebar .ui.menu {
                        margin: 2em 0 0;
                        font-size: 16px;
                    }

                    #sidebar .ui.menu > a.item {
                        color: #0e8ce4;
                        border-radius: 0 !important;
                    }

                    #sidebar .ui.menu > a.item.active {
                        background-color: #0e8ce4;
                        color: white;
                        border: none !important;
                    }

                    #sidebar .ui.menu > a.item:hover {
                        background-color: #0e8ce4;
                        color: white;
                    }

                    #content {
                        margin-right: 19%;
                        width: 81%;
                        margin-top: 3em;
                        padding-right: 3em;
                        float: right;
                    }

                    #content > .ui.grid {
                        padding-left: 4em;
                        padding-bottom: 3em;
                    }

                    #content h1 {
                        font-size: 36px;
                    }

                    #content .ui.divider:not(.hidden) {
                        margin: 0;
                    }

                    #content table.ui.table {
                        border: none;
                    }

                    #content table.ui.table thead th {
                        border-bottom: 2px solid #eee !important;
                    }
                </style>`
    contentTag.append(content);
}