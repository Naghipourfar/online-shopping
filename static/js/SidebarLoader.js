function loadSidebar(currentPage, userType = "User") {
    let sidebarTag = $("#sidebar_menu");
    let aTag;
    if (userType === "User") {
        if (currentPage === "Dashboard") {
            aTag = '<a class="active item" href="http://127.0.0.1:8000/advertisement/dashboard_buyer">';
        } else {
            aTag = '<a class="item" href="http://127.0.0.1:8000/advertisement/dashboard_buyer">';
        }
        aTag += "داشبورد";
        aTag += '</a>';
        sidebarTag.append(aTag);

        if (currentPage === "EditProfile") {
            aTag = '<a class="active item" href="http://127.0.0.1:8000/advertisement/buyer_edit_profile">';
        } else {
            aTag = '<a class="item" href="http://127.0.0.1:8000/advertisement/buyer_edit_profile">';
        }

        aTag += "ویرایش پروفایل";
        aTag += '</a>';
        sidebarTag.append(aTag);

        if (currentPage === "ManageAddresses") {
            aTag = '<a class="active item" href="http://127.0.0.1:8000/advertisement/manage_address">';
        } else {
            aTag = '<a class="item" href="http://127.0.0.1:8000/advertisement/manage_address">';
        }

        aTag += "مدیریت آدرس‌ها";
        aTag += '</a>';
        sidebarTag.append(aTag);

        if (currentPage === "BoughtProducts") {
            aTag = '<a class="active item" href="http://127.0.0.1:8000/advertisement/purchase_list">';
        } else {
            aTag = '<a class="item" href="http://127.0.0.1:8000/advertisement/purchase_list">';
        }

        aTag += "نمایش کالاهای خریداری شده";
        aTag += '</a>';
        sidebarTag.append(aTag);
    } else if (userType === "Store") {
        if (currentPage === "Dashboard") {
            aTag = '<a class="active item" href="http://127.0.0.1:8000/advertisement/dashboard_seller">';
        } else {
            aTag = '<a class="item" href="http://127.0.0.1:8000/advertisement/dashboard_seller">';
        }
        aTag += "داشبورد";
        aTag += '</a>';
        sidebarTag.append(aTag);

        if (currentPage === "EditProfile") {
            aTag = '<a class="active item" href="http://127.0.0.1:8000/advertisement/seller_edit_profile">';
        } else {
            aTag = '<a class="item" href="http://127.0.0.1:8000/advertisement/seller_edit_profile">';
        }
        aTag += "ویرایش پروفایل";
        aTag += '</a>';
        sidebarTag.append(aTag);

        if (currentPage === "ManageProducts") {
            aTag = '<a class="active item" href="http://127.0.0.1:8000/advertisement/manage_advertisement">';
        } else {
            aTag = '<a class="item" href="http://127.0.0.1:8000/advertisement/manage_advertisement">';
        }

        aTag += "مدیریت کالاها";
        aTag += '</a>';
        sidebarTag.append(aTag);

        if (currentPage === "SalesInfo") {
            aTag = '<a class="active item" href="http://127.0.0.1:8000/advertisement/store_sales_info">';
        } else {
            aTag = '<a class="item" href="http://127.0.0.1:8000/advertisement/store_sales_info">';
        }

        aTag += "میزان فروش";
        aTag += '</a>';
        sidebarTag.append(aTag);

        if (currentPage === "ChargeWallet") {
            aTag = '<a class="active item" href="http://127.0.0.1:8000/advertisement/charge_wallet">';
        } else {
            aTag = '<a class="item" href="http://127.0.0.1:8000/advertisement/charge_wallet">';
        }

        aTag += "شارژ موجودی حساب";
        aTag += '</a>';
        sidebarTag.append(aTag);

        if (currentPage === "ManageCampaigns") {
            aTag = '<a class="active item" href="http://127.0.0.1:8000/advertisement/manage_campaigns">';
        } else {
            aTag = '<a class="item" href="http://127.0.0.1:8000/advertisement/manage_campaigns">';
        }

        aTag += "مدیریت کمپین‌ها";
        aTag += '</a>';
        sidebarTag.append(aTag);

        if (currentPage === "ManageAdvertisements") {
            aTag = '<a class="active item" href="http://127.0.0.1:8000/advertisement/manage_notices">';
        } else {
            aTag = '<a class="item" href="http://127.0.0.1:8000/advertisement/manage_notices">';
        }

        aTag += "مدیریت تبلیغات";
        aTag += '</a>';
        sidebarTag.append(aTag);

    } else if (userType === "Admin") {
        if (currentPage === "Dashboard") {
            aTag = '<a class="active item" href="http://127.0.0.1:8000/advertisement/dashboard_admin">';
        } else {
            aTag = '<a class="item" href="http://127.0.0.1:8000/advertisement/dashboard_admin">';
        }
        aTag += "داشبورد";
        aTag += '</a>';
        sidebarTag.append(aTag);

        if (currentPage === "ManageUsers") {
            aTag = '<a class="active item" href="http://127.0.0.1:8000/advertisement/buyer_list">';
        } else {
            aTag = '<a class="item" href="http://127.0.0.1:8000/advertisement/buyer_list">';
        }

        aTag += "مدیریت کاربران";
        aTag += '</a>';
        sidebarTag.append(aTag);

        if (currentPage === "ManageStores") {
            aTag = '<a class="active item" href="http://127.0.0.1:8000/advertisement/seller_list">';
        } else {
            aTag = '<a class="item" href="http://127.0.0.1:8000/advertisement/seller_list">';
        }

        aTag += "مدیریت فروشگاه‌ها";
        aTag += '</a>';
        sidebarTag.append(aTag);

        if (currentPage === "ManageProductTypes") {
            aTag = '<a class="active item" href="http://127.0.0.1:8000/advertisement/category_management">';
        } else {
            aTag = '<a class="item" href="http://127.0.0.1:8000/advertisement/category_management">';
        }

        aTag += "مدیریت دسته‌بندی کالاها";
        aTag += '</a>';
        sidebarTag.append(aTag);

        if (currentPage === "ManageProducts") {
            aTag = '<a class="active item" href="http://127.0.0.1:8000/advertisement/manage_advertisement">';
        } else {
            aTag = '<a class="item" href="http://127.0.0.1:8000/advertisement/manage_advertisement">';
        }

        aTag += "مدیریت کالاها";
        aTag += '</a>';
        sidebarTag.append(aTag);

        if (currentPage === "SalesInfo") {
            aTag = '<a class="active item" href="http://127.0.0.1:8000/advertisement/sell_statistic">';
        } else {
            aTag = '<a class="item" href="http://127.0.0.1:8000/advertisement/sell_statistic">';
        }

        aTag += "اطلاعات فروش";
        aTag += '</a>';
        sidebarTag.append(aTag);
    }
}