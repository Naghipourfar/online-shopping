$(document).ready(function () {
});

function loadHeader(type){
    var row;
    if (type == "User") {
        row = '<a class="header item" href="http://127.0.0.1:8000/advertisement/dashboard_buyer">';
        row += 'مدیریت کاربر';
        row += '</a>';
        $("#header").append(row)
    } else if (type == "Admin") {
        row = '<a class="header item" href="http://127.0.0.1:8000/advertisement/dashboard_admin">';
        row += 'مدیریت سایت';
        row += '</a>';
        $("#header").append(row)
    } else {
        row = '<a class="header item" href="http://127.0.0.1:8000/advertisement/dashboard_seller">';
        row += 'مدیریت فروشگاه';
        row += '</a>';
        $("#header").append(row)
    }
    row = '<a class="header right item" onclick="signout()">خروج</a>';
    $("#header").append(row);
}

function signout() {
    localStorage.clear();
    window.location.replace("http://127.0.0.1:8000/signout");
}