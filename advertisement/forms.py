from django.db.models import Q
from django.forms import ModelForm, SelectDateWidget, DateField
from django import forms
from django.contrib.auth.models import User

from advertisement.constant import COLOR
from advertisement.models import Seller, Buyer, Address, Category, Merchant, Advertisement, Campaign, Notice
from advertisement.validators import phone_validator
from django.contrib.admin.widgets import AdminDateWidget


class SellerRegisterForm(ModelForm):
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = Seller
        exclude = ['user', 'wallet']

    def __init__(self, *args, **kwargs):
        super(SellerRegisterForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs['placeholder'] = 'نام'
        self.fields['last_name'].widget.attrs['placeholder'] = 'نام خانوادگی'
        self.fields['phone'].widget.attrs['placeholder'] = 'شماره موبایل'
        self.fields['email'].widget.attrs['placeholder'] = 'آدرس پست الکترونیکی'
        self.fields['username'].widget.attrs['placeholder'] = 'نام کاربری'
        self.fields['password'].widget.attrs['placeholder'] = 'رمز عبور'
        self.fields['company_name'].widget.attrs['placeholder'] = 'نام شرکت'
        self.fields['first_name'].widget.attrs['class'] = 'form-input '
        self.fields['last_name'].widget.attrs['class'] = 'form-input '
        self.fields['phone'].widget.attrs['class'] = 'form-input '
        self.fields['email'].widget.attrs['class'] = 'form-input '
        self.fields['company_name'].widget.attrs['class'] = 'form-input '
        self.fields['username'].widget.attrs['class'] = 'form-input '
        self.fields['password'].widget.attrs['class'] = 'form-input '
        self.fields['email'].error_messages = {'invalid': 'آدرس ایمیل معتبر نیست.'}

    def clean(self):
        data = self.cleaned_data
        if phone_validator(data['phone'])[0] is False:
            self._errors['phone'] = phone_validator(data['phone'])[1]
        if len(User.objects.filter(username=data['username'])) > 0:
            self.add_error('username', 'کاربر با این نام کاربری وجود دارد.')
        return data

    def save(self, commit=True):
        instance = super(SellerRegisterForm, self).save(commit=False)
        user_instance = User()
        user_instance.username = self.cleaned_data['username']
        user_instance.set_password(self.cleaned_data['password'])
        user_instance.save()
        instance.user = user_instance
        instance.wallet = 0
        if commit:
            instance.save()
        return instance


class BuyerRegisterForm(ModelForm):
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = Buyer
        exclude = ['user']

    def __init__(self, *args, **kwargs):
        super(BuyerRegisterForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs['placeholder'] = 'نام'
        self.fields['last_name'].widget.attrs['placeholder'] = 'نام خانوادگی'
        self.fields['phone'].widget.attrs['placeholder'] = 'شماره موبایل'
        self.fields['email'].widget.attrs['placeholder'] = 'آدرس پست الکترونیکی'
        self.fields['age'].widget.attrs['placeholder'] = 'سن'
        self.fields['username'].widget.attrs['placeholder'] = 'نام کاربری'
        self.fields['password'].widget.attrs['placeholder'] = 'رمز عبور'
        self.fields['first_name'].widget.attrs['class'] = 'form-input '
        self.fields['last_name'].widget.attrs['class'] = 'form-input '
        self.fields['phone'].widget.attrs['class'] = 'form-input '
        self.fields['email'].widget.attrs['class'] = 'form-input '
        self.fields['sex'].widget.attrs['class'] = 'form-input '
        self.fields['age'].widget.attrs['class'] = 'form-input '
        self.fields['username'].widget.attrs['class'] = 'form-input '
        self.fields['password'].widget.attrs['class'] = 'form-input '
        self.fields['email'].error_messages = {'invalid': 'آدرس ایمیل معتبر نیست.'}

    def clean(self):
        data = self.cleaned_data
        if phone_validator(data['phone'])[0] is False:
            self.add_error('phone', phone_validator(data['phone'])[1])
        if len(User.objects.filter(username=data['username'])) > 0:
            self.add_error('username', 'کاربر با این نام کاربری وجود دارد.')
        return data

    def save(self, commit=True):
        instance = super(BuyerRegisterForm, self).save(commit=False)
        user_instance = User()
        user_instance.username = self.cleaned_data['username']
        user_instance.set_password(self.cleaned_data['password'])
        user_instance.save()
        instance.user = user_instance
        if commit:
            instance.save()
        return instance


class LoginForm(forms.Form):
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['placeholder'] = 'نام کاربری'
        self.fields['username'].widget.attrs['class'] = 'form-input '
        self.fields['password'].widget.attrs['placeholder'] = 'رمز عبور'
        self.fields['password'].widget.attrs['class'] = 'form-input '

    def clean(self):
        data = self.cleaned_data
        return data


class ResetPasswordForm(forms.Form):
    email = forms.EmailField()

    def __init__(self, *args, **kwargs):
        super(ResetPasswordForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs['placeholder'] = 'آدرس پست الکترونیکی'
        self.fields['email'].widget.attrs['class'] = 'form-input '


class SubmitPassword(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    def __init__(self, *args, **kwargs):
        super(SubmitPassword, self).__init__(*args, **kwargs)
        self.fields['password'].widget.attrs['placeholder'] = 'رمز عبور'
        self.fields['password'].widget.attrs['class'] = 'form-input '

        self.fields['confirm_password'].widget.attrs['placeholder'] = 'تکرار رمز عبور'
        self.fields['confirm_password'].widget.attrs['class'] = 'form-input '

    def clean(self):
        cleaned_data = super(SubmitPassword, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")
        if password != confirm_password:
            self.add_error('password', 'تکرار رمز عبور با رمز عبور تطابق ندارد.')
        return cleaned_data


class AddressForm(ModelForm):
    class Meta:
        model = Address
        exclude = ['merchant']
        labels = {
            "city": "شهر",
            "district": "منطقه",
            "street": "خیابان",
            "house_number": "پلاک",
            "unit": "واحد"
        }

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(AddressForm, self).__init__(*args, **kwargs)
        self.fields['city'].widget.attrs['placeholder'] = 'شهر'
        self.fields['district'].widget.attrs['placeholder'] = 'منطقه'
        self.fields['street'].widget.attrs['placeholder'] = 'خیابان'
        self.fields['house_number'].widget.attrs['placeholder'] = 'پلاک'
        self.fields['unit'].widget.attrs['placeholder'] = 'واحد'
        self.fields['city'].widget.attrs['class'] = 'form-control '
        self.fields['district'].widget.attrs['class'] = 'form-control '
        self.fields['street'].widget.attrs['class'] = 'form-control '
        self.fields['house_number'].widget.attrs['class'] = 'form-control '
        self.fields['unit'].widget.attrs['class'] = 'form-control '

    def save(self, commit=True):
        instance = super(AddressForm, self).save(commit=False)
        instance.merchant = Merchant.objects.filter(user=self.request.user)[0]
        if commit:
            instance.save()
        return instance


class CategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = '__all__'
        labels = {
            "title": "نام دسته‌بندی",
            "parent": "دسته‌بندی پدر",
            "percentile": "درصد سود فروش"
        }

    def __init__(self, *args, **kwargs):
        super(CategoryForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs['placeholder'] = 'نام دسته‌بندی'
        self.fields['parent'].widget.attrs['placeholder'] = 'دسته‌بندی پدر'
        self.fields['percentile'].widget.attrs['placeholder'] = 'درصد سود فروش'
        self.fields['title'].widget.attrs['class'] = 'form-control '
        self.fields['parent'].widget.attrs['class'] = 'form-control '


class AdminAddAdvertisementForm(ModelForm):
    class Meta:
        model = Advertisement
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(AdminAddAdvertisementForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs['placeholder'] = 'نام'
        self.fields['price'].widget.attrs['placeholder'] = 'قیمت'
        self.fields['description'].widget.attrs['placeholder'] = 'توضیحات'
        self.fields['seller'].widget.attrs['placeholder'] = 'فروشنده'

        self.fields['title'].label = 'نام'
        self.fields['price'].label = 'قیمت'
        self.fields['description'].label = 'توضیحات'
        self.fields['category'].label = 'دسته‌بندی'
        self.fields['profile_image'].label = 'تصویر کالا'
        self.fields['seller'].label = 'فروشنده'
        self.fields['color'].label = 'رنگ'

        self.fields['title'].widget.attrs['class'] = 'form-control '
        self.fields['price'].widget.attrs['class'] = 'form-control '
        self.fields['description'].widget.attrs['class'] = 'form-control '
        self.fields['seller'].widget.attrs['class'] = 'form-control '
        self.fields['seller'].queryset = Seller.objects.all()
        self.fields['category'].queryset = Category.objects.exclude(
            id__in=Category.objects.filter(parent__isnull=False).values('parent_id'))


class BuyerEditProfileForm(ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = Buyer
        exclude = ['user']

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(BuyerEditProfileForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = 'نام'
        self.fields['last_name'].label = 'نام خانوادگی'
        self.fields['phone'].label = 'شماره موبایل'
        self.fields['age'].label = 'سن'
        self.fields['sex'].label = 'جنسیت'
        self.fields['email'].label = 'ایمیل'
        self.fields['password'].label = 'رمز عبور'
        self.fields['confirm_password'].label = 'تکرار رمز عبور'
        self.fields['first_name'].widget.attrs['class'] = 'form-input '
        self.fields['last_name'].widget.attrs['class'] = 'form-input '
        self.fields['phone'].widget.attrs['class'] = 'form-input '
        self.fields['sex'].widget.attrs['class'] = 'form-input '
        self.fields['age'].widget.attrs['class'] = 'form-input '
        self.fields['password'].widget.attrs['class'] = 'form-input '
        self.fields['confirm_password'].widget.attrs['class'] = 'form-input '

        self.fields['email'].widget.attrs['readonly'] = True

    def clean(self):
        cleaned_data = super(BuyerEditProfileForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")
        if password != confirm_password:
            self.add_error('password', 'تکرار رمز عبور با رمز عبور تطابق ندارد.')
        return cleaned_data

    def save(self, commit=True):
        instance = super(BuyerEditProfileForm, self).save(commit=False)
        instance.user = self.user
        instance.user.set_password(self.cleaned_data['password'])
        instance.user.save()
        if commit:
            instance.save()
        return instance


class SellerEditProfileForm(ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = Seller
        exclude = ['user']

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(SellerEditProfileForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = 'نام'
        self.fields['last_name'].label = 'نام خانوادگی'
        self.fields['phone'].label = 'شماره موبایل'
        self.fields['company_name'].label = 'نام شرکت'
        self.fields['email'].label = 'ایمیل'
        self.fields['password'].label = 'رمز عبور'
        self.fields['confirm_password'].label = 'تکرار رمز عبور'
        self.fields['wallet'].label = 'موجودی حساب'
        self.fields['first_name'].widget.attrs['class'] = 'form-input '
        self.fields['last_name'].widget.attrs['class'] = 'form-input '
        self.fields['phone'].widget.attrs['class'] = 'form-input '
        self.fields['company_name'].widget.attrs['class'] = 'form-input '
        self.fields['password'].widget.attrs['class'] = 'form-input '
        self.fields['confirm_password'].widget.attrs['class'] = 'form-input '

        self.fields['email'].widget.attrs['readonly'] = True
        self.fields['wallet'].widget.attrs['readonly'] = True

    def clean(self):
        cleaned_data = super(SellerEditProfileForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")
        if password != confirm_password:
            self.add_error('password', 'تکرار رمز عبور با رمز عبور تطابق ندارد.')
        return cleaned_data

    def save(self, commit=True):
        instance = super(SellerEditProfileForm, self).save(commit=False)
        instance.user = self.user
        instance.user.set_password(self.cleaned_data['password'])
        instance.user.save()
        if commit:
            instance.save()
        return instance


class AddAdvertisementForm(ModelForm):
    class Meta:
        model = Advertisement
        exclude = ['seller']

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(AddAdvertisementForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs['placeholder'] = 'نام'
        self.fields['price'].widget.attrs['placeholder'] = 'قیمت'
        self.fields['description'].widget.attrs['placeholder'] = 'توضیحات'

        self.fields['title'].label = 'نام'
        self.fields['price'].label = 'قیمت'
        self.fields['description'].label = 'توضیحات'
        self.fields['category'].label = 'دسته‌بندی'
        self.fields['profile_image'].label = 'تصویر کالا'
        self.fields['color'].label = 'رنگ'

        self.fields['title'].widget.attrs['class'] = 'form-control '
        self.fields['price'].widget.attrs['class'] = 'form-control '
        self.fields['description'].widget.attrs['class'] = 'form-control '
        self.fields['category'].queryset = Category.objects.exclude(
            id__in=Category.objects.filter(parent__isnull=False).values('parent_id'))

    def save(self, commit=True):
        instance = super(AddAdvertisementForm, self).save(commit=False)
        instance.seller = Seller.objects.filter(user=self.request.user)[0]
        if commit:
            instance.save()
        return instance


class AdvertisementEditForm(ModelForm):
    class Meta:
        model = Advertisement
        exclude = ['seller']

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(AdvertisementEditForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs['placeholder'] = 'نام'
        self.fields['price'].widget.attrs['placeholder'] = 'قیمت'
        self.fields['description'].widget.attrs['placeholder'] = 'توضیحات'

        self.fields['title'].label = 'نام'
        self.fields['price'].label = 'قیمت'
        self.fields['description'].label = 'توضیحات'
        self.fields['category'].label = 'دسته‌بندی'
        self.fields['profile_image'].label = 'تصویر کالا'
        self.fields['color'].label = 'رنگ'

        self.fields['title'].widget.attrs['class'] = 'form-control '
        self.fields['price'].widget.attrs['class'] = 'form-control '
        self.fields['description'].widget.attrs['class'] = 'form-control '
        self.fields['category'].queryset = Category.objects.exclude(
            id__in=Category.objects.filter(parent__isnull=False).values('parent_id'))

    def save(self, commit=True):
        instance = super(AdvertisementEditForm, self).save(commit=False)
        instance.seller = Seller.objects.filter(user=self.user)[0]
        if commit:
            instance.save()
        return instance


class SearchForm(forms.Form):
    title = forms.CharField(required=False, max_length=200,
                            widget=forms.TextInput(
                                attrs={'class': 'header_search_input', 'placeholder': 'جست و جوی کالا'}))


class WalletForm(forms.Form):
    value = forms.IntegerField(required=False, widget=forms.TextInput(
        attrs={'placeholder': 'مقدار افزایش حساب'}
    ))
    def __init__(self, *args, **kwargs):
        super(WalletForm, self).__init__(*args, **kwargs)
        self.fields['value'].label = 'افزایش حساب'


class SellerForm(forms.Form):
    title = forms.CharField(required=False, max_length=200,
                            widget=forms.TextInput(
                                attrs={'placeholder': 'نام شرکت'}))

    def __init__(self, *args, **kwargs):
        super(SellerForm, self).__init__(*args, **kwargs)
        self.fields['title'].label = 'نام شرکت'


class AdvanceSearchForm(forms.Form):
    minimum_price = forms.IntegerField(label='حداقل قیمت', required=False,
                                       widget=forms.TextInput(
                                           attrs={'class': 'form-control',
                                                  'placeholder': 'حداقل قیمت',
                                                  'id':'value'}))
    maximum_price = forms.IntegerField(label='حداکثر قیمت', required=False,
                                       widget=forms.TextInput(
                                           attrs={'class': 'form-control',
                                                  'placeholder': 'حداکثر قیمت',
                                                  'id':'value'}))
    color = forms.ChoiceField(label='رنگ', required=False, choices=COLOR, widget=forms.Select(attrs={'class':'color'}))
    seller = forms.ModelChoiceField(label='فروشنده', required=False, queryset=Seller.objects.all(), widget=forms.Select(attrs={'class':'seller'}))

    def __init__(self, *args, **kwargs):
        super(AdvanceSearchForm, self).__init__(*args, **kwargs)
        self.fields['seller'].widget.attrs['class'] = 'form-input '
        self.fields['color'].widget.attrs['class'] = 'form-input '


    def clean(self):
        cleaned_data = super().clean()
        minimum_price = cleaned_data.get("minimum_price")
        maximum_price = cleaned_data.get("maximum_price")
        if minimum_price is not None and maximum_price is not None and minimum_price > maximum_price:
            self._errors['minimum_price'] = 'حداقل قیمت باید کمتر از حداکثر قیمت باشد'
        return cleaned_data


class AddCampaignForm(ModelForm):
    class Meta:
        model = Campaign
        fields ='__all__'

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(AddCampaignForm, self).__init__(*args, **kwargs)
        self.fields['start_date'].widget = SelectDateWidget(
            empty_label=("انتخاب سال", "انتخاب ماه", "انتخاب روز"),
        )
        self.fields['end_date'].widget = SelectDateWidget(
            empty_label=("انتخاب سال", "انتخاب ماه", "انتخاب روز"),
        )


        self.fields['discount'].widget.attrs['placeholder'] = 'درصد تخفیف'

        self.fields['discount'].label = 'درصد تخفیف'
        self.fields['advertisement'].label = 'کالا'
        self.fields['start_date'].label = 'شروع کمپین'
        self.fields['end_date'].label = 'پایان کمپین'

        self.fields['advertisement'].queryset = Advertisement.objects.filter(seller__user=self.user)


    def clean(self):
        cleaned_data = super(AddCampaignForm, self).clean()
        start_date = cleaned_data.get("start_date")
        end_date = cleaned_data.get("end_date")
        if end_date < start_date:
            self._errors['end_date'] = 'تاریخ پایان نباید قبل از تاریخ شروع باشد'
        campaigns = Campaign.objects.filter(advertisement=cleaned_data.get("advertisement"))
        for campaign in campaigns:
            if max(cleaned_data.get("start_date"), campaign.start_date) <= min(cleaned_data.get("end_date"), campaign.end_date):
                self._errors['advertisement'] = 'در این تاریخ برای این کالا تخفیف موجود است.'
        return cleaned_data



class AddNoticeForm(ModelForm):
    class Meta:
        model = Notice
        fields ='__all__'

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(AddNoticeForm, self).__init__(*args, **kwargs)
        self.fields['start_date'].widget = SelectDateWidget(
            empty_label=("انتخاب سال", "انتخاب ماه", "انتخاب روز"),
        )
        self.fields['end_date'].widget = SelectDateWidget(
            empty_label=("انتخاب سال", "انتخاب ماه", "انتخاب روز"),
        )

        self.fields['advertisement'].label = 'کالا'
        self.fields['start_date'].label = 'تاریخ شروع'
        self.fields['end_date'].label = 'تاریخ پایان'
        self.fields['keywords'].label = 'کلمه‌های کلیدی جدا شده با فاصله'

        self.fields['advertisement'].queryset = Advertisement.objects.filter(seller__user=self.user)


    def clean(self):
        cleaned_data = super(AddNoticeForm, self).clean()
        start_date = cleaned_data.get("start_date")
        end_date = cleaned_data.get("end_date")
        if end_date < start_date:
            self._errors['end_date'] = 'تاریخ پایان نباید قبل از تاریخ شروع باشد'
        return cleaned_data

