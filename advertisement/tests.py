import datetime
from django.urls import reverse

from django.contrib.auth.models import User
from django.test import RequestFactory
from django.test import TestCase

from advertisement.forms import SellerRegisterForm, BuyerRegisterForm, AddressForm, LoginForm, SubmitPassword, \
    CategoryForm, AdminAddAdvertisementForm, BuyerEditProfileForm, SellerEditProfileForm, AddAdvertisementForm, \
    AdvertisementEditForm, SearchForm, WalletForm, AdvanceSearchForm, AddNoticeForm
from advertisement.models import Seller, Buyer, Address, Advertisement, Category, Transaction, Campaign, Notice
from advertisement.views import signin, dashboard_buyer, dashboard_admin, delete_seller, reorder_ads, get_category_list, \
    search


class TestAdvertisementModels(TestCase):

    def create_user(self, **kwargs):
        user = User()
        user.username = kwargs['username']
        user.set_password(kwargs['password'])
        user.save()
        return user

    def create_seller(self, **kwargs):
        user = self.create_user(**kwargs)

        del kwargs['username']
        del kwargs['password']

        seller = Seller(user=user, **kwargs)
        seller.save()
        return seller

    def create_buyer(self, **kwargs):
        user = self.create_user(**kwargs)

        del kwargs['username']
        del kwargs['password']

        buyer = Buyer(user=user, **kwargs)
        buyer.save()
        return buyer

    def create_address(self, **kwargs):
        address = Address(**kwargs)
        address.save()
        return address

    def create_campaign(self, **kwargs):
        campaign = Campaign(**kwargs)
        campaign.save()
        return campaign

    def create_notice(self, **kwargs):
        notice = Notice(**kwargs)
        notice.save()
        return notice

    def test_seller(self):
        seller = self.create_seller(username='testuser', password='testpass', first_name='test', last_name='testi',
                                    email='testggkalatest@gmail.com', phone='09124683019',
                                    company_name='testcompany', wallet=1)
        self.assertEqual(seller.persian_phone_number(), '۰۹۱۲۴۶۸۳۰۱۹')
        self.assertEqual(Seller.objects.count(), 1)
        self.assertEqual(seller.phone, '09124683019')
        self.assertEqual(seller.company_name, 'testcompany')
        self.assertFalse(WalletForm().is_valid())
        self.assertEqual(seller.user.username, 'testuser')
        self.assertEqual(seller.get_wallet(), '۱')

    def test_buyer(self):
        buyer = self.create_buyer(username='testuser', password='testpass', first_name='test', last_name='testi',
                                  email='testggkalatest@gmail.com', phone='09124683019',
                                  age=29, sex='F')
        self.assertEqual(Buyer.objects.count(), 1)
        self.assertEqual(buyer.sex, 'F')
        self.assertEqual(buyer.last_name, 'testi')
        self.assertFalse(SearchForm().is_valid())
        self.assertEqual(buyer.user.username, 'testuser')
        self.assertEqual(buyer.persian_age(), '۲۹')

    #
    def test_address(self):
        buyer = self.create_buyer(username='testuser', password='testpass', first_name='test', last_name='testi',
                                  email='testggkalatest@gmail.com', phone='09124683019',
                                  age=29, sex='F')
        address = self.create_address(city='karaj', district='gohardasht', street='golestan12', house_number='47',
                                      unit='7', merchant=buyer)
        self.assertEqual(address.persian_house_number(), '۴۷')
        self.assertEqual(address.persian_unit(), '۷')
        self.assertEqual(Address.objects.count(), 1)
        self.assertEqual(address.city, 'karaj')
        self.assertEqual(address.unit, '7')
        self.assertEqual(address.merchant.sex, 'F')
        self.assertEqual(address.merchant.user.username, 'testuser')

    def create_category(self, title, parent, percentile):
        category = Category(title=title, parent=parent, percentile=percentile)
        category.save()
        return category

    def create_advertisement(self, title, price, description, profile_image, category, seller, color):
        advertisement = Advertisement(title=title, description=description, price=price, category=category,
                                      seller=seller, color=color)
        advertisement.save()
        return advertisement

    def test_category(self):
        category = self.create_category('car', None, 10)
        self.assertEqual(Category.objects.count(), 1)
        self.assertEqual(category.title, 'car')
        second_category = self.create_category('pride', category, 5)
        self.assertEqual(Category.objects.count(), 2)
        self.assertEqual(second_category.parent, category)
        self.assertEquals(second_category.parent.title, 'car')
        self.assertEqual(category.persian_percentile(), '۱۰')

    def test_advertisement(self):
        seller = self.create_seller(username='testuser', password='testpass', first_name='test', last_name='testi',
                                    email='testggkalatest@gmail.com', phone='09124683019',
                                    company_name='testcompany', wallet=1)
        category = self.create_category('car', None, 4)
        advertisement = self.create_advertisement('test_ad', 20000, 'test_description', None, category, seller, 'R')
        self.assertEquals(Advertisement.objects.count(), 1)
        self.assertEquals(advertisement.title, 'test_ad')
        self.assertEquals(advertisement.price, 20000)
        self.assertEquals(advertisement.description, 'test_description')
        self.assertEquals(advertisement.category, category)
        self.assertFalse(AdminAddAdvertisementForm().is_valid())
        self.assertFalse(AdvanceSearchForm().is_valid())
        self.assertEquals(advertisement.seller.first_name, 'test')
        self.assertEquals(advertisement.color, 'R')
        self.assertFalse(AddAdvertisementForm().is_valid())
        self.assertEqual(advertisement.persian_price(), '۲۰۰۰۰')

    def create_transaction(self, advertisement, buyer, address, time):
        transaction = Transaction(advertisement=advertisement, buyer=buyer, address=address, time=time)
        transaction.save()
        return transaction

    def test_transaction(self):
        buyer = self.create_buyer(username='testuserb', password='testpass', first_name='test_', last_name='testi',
                                  email='testggkalatest@gmail.com', phone='09124683019',
                                  age=29, sex='F')
        address = self.create_address(city='karaj', district='gohardasht', street='golestan12', house_number='47',
                                      unit='7', merchant=buyer)
        category = self.create_category('car', None, 3)
        seller = self.create_seller(username='testusers', password='testpass', first_name='test_', last_name='testi',
                                    email='testseller@gmail.com', phone='09124683019',
                                    company_name='testcompany', wallet=1)
        advertisement = self.create_advertisement('test_ad', 20000, 'test_description', None, category, seller, 'B')
        transaction = self.create_transaction(advertisement, buyer, address, 'test_time')
        self.assertEqual(Transaction.objects.count(), 1)
        self.assertEqual(transaction.buyer.first_name, 'test_')
        self.assertFalse(SellerEditProfileForm(user=buyer.user).is_valid())
        self.assertEqual(transaction.advertisement.seller.first_name, 'test_')
        self.assertEqual(transaction.advertisement.category, category)
        self.assertEqual(transaction.time, 'test_time')
        self.assertFalse(BuyerEditProfileForm(user=buyer.user).is_valid())

    def test_permission(self):
        buyer = self.create_buyer(username='testuser', password='testpass', first_name='test', last_name='testi',
                                  email='testggkalatest@gmail.com', phone='09124683019',
                                  age=29, sex='F')
        self.client.force_login(buyer.user)

        request = RequestFactory().post(reverse('dashboard_buyer'))
        request.user = buyer.user

        self.assertEqual(dashboard_buyer(request).status_code, 200)

        request = RequestFactory().post(reverse('dashboard_admin'))
        request.user = buyer.user

        self.assertEqual(dashboard_admin(request).status_code, 302)

    def test_dashboard(self):
        my_admin = User.objects.create_superuser('myuser', 'myemail@test.com', '123456')
        my_admin.save()
        seller = self.create_seller(username='testuser', password='testpass', first_name='test', last_name='testi',
                                    email='testggkalatest@gmail.com', phone='09124683019',
                                    company_name='testcompany', wallet=1)
        self.client.force_login(my_admin)
        request = RequestFactory().post(reverse('delete_seller', args=[seller.pk]))
        self.assertFalse(AdvertisementEditForm(user=my_admin).is_valid())
        self.assertFalse(AddNoticeForm(user=my_admin).is_valid())
        request.user = my_admin
        self.assertEqual(delete_seller(request, seller.pk).status_code, 302)

    def test_reorder(self):
        seller = self.create_seller(username='testuser', password='testpass', first_name='test', last_name='testi',
                                    email='testggkalatest@gmail.com', phone='09124683019',
                                    company_name='testcompany', wallet=1)
        category = self.create_category('car', None, 4)
        advertisement = self.create_advertisement('test_ad1', 20000, 'test_description', None, category, seller, 'B')
        advertisement2 = self.create_advertisement('test_ad2', 20000, 'test_description', None, category, seller, 'B')
        list_ads = [advertisement,advertisement2]
        new_ads = reorder_ads(list_ads)
        for i in range(len(new_ads)):
            self.assertEqual(new_ads[i][0],list_ads[i])

    def test_category_list(self):
        category = self.create_category('car', None, 4)
        category2 = self.create_category('laptop', None, 4)
        self.assertEqual(len(get_category_list(None)),2)

    def test_search(self):
        buyer = self.create_buyer(username='testuser', password='testpass', first_name='test', last_name='testi',
                                  email='testggkalatest@gmail.com', phone='09124683019',
                                  age=29, sex='F')
        self.client.force_login(buyer.user)
        request = RequestFactory().post(reverse('search'))
        request.user = buyer.user
        self.assertEqual(search(request).status_code, 200)


    def test_seller_forms(self):
        form_data = {'first_name': 'testfirst', 'last_name': 'testlast', 'phone':'09124683019','company_name':'testcompany','email':'testggkalatest@gmail.com', 'username':'testuser', 'password':'testpass'}
        form = SellerRegisterForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_buyer_forms(self):
        form_data = {'first_name': 'testfirst', 'last_name': 'testlast', 'phone':'09124683019','age':23, 'sex': 'F', 'email':'testggkalatest@gmail.com', 'username':'testuser', 'password':'testpass'}
        form = BuyerRegisterForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_address_form(self):
        form_data = {'city':'karaj', 'district':'azimiye', 'unit':'6', 'street':'neda', 'house_number':'17'}
        form = AddressForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_login_form(self):
        form_data = {'username':'testuser', 'password':'testpass'}
        form = LoginForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_submitPassword_form(self):
        form_data = {'confirm_password': 'testpass1', 'password': 'testpass'}
        form = SubmitPassword(data=form_data)
        self.assertFalse(form.is_valid())

    def test_category_form(self):
        category = self.create_category('car', None, 4)
        form_data = {'title': 'cat', 'parent': category, 'percentile':23}
        form = CategoryForm(data=form_data)
        self.assertFalse(form.is_valid())















