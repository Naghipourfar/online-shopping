from django.contrib import admin

from advertisement.models import Merchant, Seller, Buyer, Address, ResetPassword, Category, Advertisement, Transaction, \
    Campaign

admin.site.register(Merchant)
admin.site.register(Seller)
admin.site.register(Buyer)
admin.site.register(Address)
admin.site.register(ResetPassword)
admin.site.register(Category)
admin.site.register(Advertisement)
admin.site.register(Transaction)
admin.site.register(Campaign)


