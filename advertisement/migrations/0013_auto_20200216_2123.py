# Generated by Django 2.1.5 on 2020-02-16 21:23

import advertisement.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('advertisement', '0012_notice'),
    ]

    operations = [
        migrations.AddField(
            model_name='notice',
            name='keywords',
            field=models.CharField(default='', max_length=1000),
        ),
        migrations.AlterField(
            model_name='advertisement',
            name='profile_image',
            field=models.ImageField(upload_to=advertisement.models.get_image_path),
        ),
    ]
