import os

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models

from advertisement.constant import SEX, COLOR
from advertisement.utils import generate_random_token


def get_image_path(instance, filename):
    return os.path.join(str(Advertisement.objects.all().count()) + filename)


class Merchant(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField(unique=True)
    phone = models.CharField(max_length=12)
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def persian_phone_number(self):
        devanagari_nums = ('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹')
        number = str(self.phone)
        persian_number = ''.join(devanagari_nums[int(digit)] for digit in number)
        return persian_number

    def __str__(self):
        return self.first_name + ' ' + self.last_name


class Address(models.Model):
    city = models.CharField(max_length=30)
    district = models.CharField(max_length=30)
    street = models.CharField(max_length=30)
    house_number = models.CharField(max_length=30)
    unit = models.CharField(max_length=30)
    merchant = models.ForeignKey('Merchant', on_delete=models.DO_NOTHING, null=True)

    def persian_unit(self):
        devanagari_nums = ('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹')
        number = str(self.unit)
        persian_number = ''.join(devanagari_nums[int(digit)] for digit in number)
        return persian_number

    def persian_house_number(self):
        devanagari_nums = ('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹')
        number = str(self.house_number)
        persian_number = ''.join(devanagari_nums[int(digit)] for digit in number)
        return persian_number

    def __str__(self):
        return self.city + ' - ' + self.district + ' - ' + self.street + ' - ' + self.persian_house_number() + ' - ' + self.persian_unit()


class Seller(Merchant):
    company_name = models.CharField(max_length=30)
    wallet = models.IntegerField()

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    def get_wallet(self):
        devanagari_nums = ('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹')
        number = str(self.wallet)
        if number.__contains__('-'):
            number = number[1:]
            persian_price = ''.join(devanagari_nums[int(digit)] for digit in number) + '-'
        else:
            persian_price = ''.join(devanagari_nums[int(digit)] for digit in number)
        return persian_price


class Buyer(Merchant):
    sex = models.CharField(max_length=1, choices=SEX)
    age = models.IntegerField()

    def persian_age(self):
        devanagari_nums = ('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹')
        number = str(self.age)
        persian_age = ''.join(devanagari_nums[int(digit)] for digit in number)
        return persian_age

    def __str__(self):
        return self.first_name + ' ' + self.last_name


class ResetPassword(models.Model):
    merchant = models.ForeignKey('Merchant', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    token = models.CharField(max_length=32, default=generate_random_token)

    def get_reset_password_link(self):
        return "{}/change_password?token={}".format(settings.SITE_DOMAIN, self.token)


class Category(models.Model):
    title = models.CharField(max_length=50)
    parent = models.ForeignKey('self', null=True, on_delete=models.CASCADE, blank=True)
    percentile = models.IntegerField()

    def __str__(self):
        return self.title

    def persian_percentile(self):
        devanagari_nums = ('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹')
        number = str(self.percentile)
        persian_number = ''.join(devanagari_nums[int(digit)] for digit in number)
        return persian_number


class Advertisement(models.Model):
    title = models.CharField(max_length=80)
    price = models.IntegerField()
    description = models.TextField()
    profile_image = models.ImageField(upload_to=get_image_path)
    category = models.ForeignKey('Category', on_delete=models.CASCADE)
    seller = models.ForeignKey('Seller', on_delete=models.CASCADE)
    color = models.CharField(max_length=1, choices=COLOR)

    def __str__(self):
        return self.title

    def persian_price(self):
        devanagari_nums = ('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹')
        number = str(self.price)
        persian_price = ''.join(devanagari_nums[int(digit)] for digit in number)
        return persian_price


class Transaction(models.Model):
    advertisement = models.ForeignKey('Advertisement', on_delete=models.CASCADE)
    buyer = models.ForeignKey('Buyer', on_delete=models.CASCADE)
    address = models.ForeignKey('Address', on_delete=models.CASCADE)
    time = models.CharField(max_length=300)

    def __str__(self):
        return self.advertisement.title + ' to ' + self.buyer.first_name + ' ' + self.buyer.last_name


class Campaign(models.Model):
    advertisement = models.ForeignKey('Advertisement', on_delete=models.CASCADE)
    start_date = models.DateField()
    end_date = models.DateField()
    discount = models.IntegerField()

    def __str__(self):
        return self.advertisement.title + ' ' + str(self.discount)


class Notice(models.Model):
    advertisement = models.ForeignKey('Advertisement', on_delete=models.CASCADE)
    start_date = models.DateField()
    end_date = models.DateField()
    keywords = models.CharField(default="", max_length=1000)

    def __str__(self):
        return self.advertisement.title
