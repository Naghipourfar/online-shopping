import datetime
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.db.models import Sum
from django.shortcuts import render, redirect
from statistics import mode

from django.contrib.auth import authenticate, login, logout
from django.template.loader import render_to_string

from advertisement.forms import SellerRegisterForm, BuyerRegisterForm, LoginForm, ResetPasswordForm, SubmitPassword, \
    AddressForm, CategoryForm, AddAdvertisementForm, AdminAddAdvertisementForm, BuyerEditProfileForm, \
    SellerEditProfileForm, AdvertisementEditForm, SearchForm, WalletForm, SellerForm, AdvanceSearchForm, AddCampaignForm, \
    AddNoticeForm
from advertisement.models import Merchant, ResetPassword, Address, Category, Buyer, Seller, Advertisement, Transaction, \
    Campaign, Notice
from advertisement.utils import send_email_async
from django.contrib.auth.decorators import user_passes_test



def reorder_ads(advertisements):
    advertisements = list(advertisements)
    notices = Notice.objects.filter(start_date__range=["2011-01-01", datetime.date.today()],
                                    end_date__range=[datetime.date.today(), "2050-01-01"])
    notice_ads = notices.values_list('advertisement', flat=True)
    ads_id = []
    for advertisement in advertisements:
        if advertisement.id in notice_ads:
            ads_id.append(advertisement.id)
    for advertisement in advertisements:
        if advertisement.id not in notice_ads:
            ads_id.append(advertisement.id)
    ads = []
    for ad_id in ads_id:
        ads.append(Advertisement.objects.filter(pk=ad_id)[0])

    campaigns = Campaign.objects.filter(start_date__range=["2011-01-01", datetime.date.today()],
                                    end_date__range=[datetime.date.today(), "2050-01-01"])

    new_ads = []
    for ad in ads:
        find = False
        for campaign in campaigns:
            if campaign.advertisement == ad:
                new_ads.append((ad, campaign.discount, (campaign.end_date - datetime.date.today()).days, to_persian_number(ad.price - campaign.discount * ad.price // 100)))
                find = True
                break
        if find is not True:
            new_ads.append((ad, None, None, None))
    return new_ads

def get_category_list(par):
    result = []
    categories = Category.objects.filter(parent=par)
    for category in categories:
        result.append((category, get_category_list(category)))
    return result


def is_admin(user):
    return not is_seller(user) and not is_buyer(user)


def is_seller(user):
    try:
        return len(Seller.objects.filter(user=user)) > 0
    except User.merchant.RelatedObjectDoesNotExist:
        return False


def is_buyer(user):
    try:
        return len(Buyer.objects.filter(user=user)) > 0
    except User.merchant.RelatedObjectDoesNotExist:
        return False


def home(request):
    return render(request, '../templates/shop.html')


def register_seller(request):
    if request.method == 'GET':
        form = SellerRegisterForm()
        return render(request, '../templates/signUp-store.html', {'form': form})
    else:
        form = SellerRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
        else:
            return render(request, '../templates/signUp-store.html', {'form': form})


def register_buyer(request):
    if request.method == 'GET':
        form = BuyerRegisterForm()
        return render(request, '../templates/signUp-user.html', {'form': form})
    else:
        form = BuyerRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
        else:
            return render(request, '../templates/signUp-user.html', {'form': form})


def signin(request):
    if request.method == 'GET':
        form = LoginForm()
        return render(request, '../templates/signin.html', {'form': form})
    else:
        form = LoginForm(request.POST)
        user = authenticate(request, username=form['username'].value(), password=form['password'].value())
        if user is not None:
            login(request, user)
            if is_admin(user):
                return redirect('dashboard_admin')
            elif is_seller(user):
                return redirect('dashboard_seller')
            elif is_buyer(user):
                return redirect('dashboard_buyer')
        else:
            form.add_error('username', 'نام کاربری یا رمز عبور اشتباه است.')
            return render(request, '../templates/signin.html', {'form': form})


def signout(request):
    logout(request)
    return redirect('home')


def reset_password(request):
    if request.method == 'GET':
        form = ResetPasswordForm()
        return render(request, '../templates/forget_password.html', {'form': form})
    else:
        form = ResetPasswordForm(request.POST, request.FILES)
        try:
            merchant = Merchant.objects.filter(email=form.data['email'])[0]
            new_password = ResetPassword(merchant=merchant)
            new_password.save()
            subject = 'فراموشی رمز عبور'
            body = render_to_string('email_template', context={
                'username': merchant.user.username,
                'reset_link': new_password.get_reset_password_link()
            })
            email = EmailMessage(subject=subject, body=body, to=[merchant.email])
            send_email_async(email)
            return redirect('home')
        except IndexError:
            form.add_error('email', 'هیچ کاربری با این ایمیل وجود ندارد.')
            return render(request, '../templates/forget_password.html', {
                'form': form})


def change_password(request):
    if request.method == 'GET':
        form = SubmitPassword()
        return render(request, '../templates/reset_password.html', {'form': form})
    else:
        form = SubmitPassword(request.POST)
        token = request.GET.get('token')
        if form.is_valid():
            new_password = ResetPassword.objects.filter(token=token)[0]
            new_password.merchant.user.set_password(form.data['password'])
            new_password.merchant.user.save()
            return redirect('home')
        else:
            return render(request, '../templates/reset_password.html', {'form': form})


@user_passes_test(is_buyer)
def add_address(request):
    if request.method == 'GET':
        form = AddressForm()
        return render(request, '../templates/UserAddNewAddress.html', {'form': form})
    else:
        form = AddressForm(request.POST, request=request)
        if form.is_valid():
            address = form.save()
            address.merchant = Merchant.objects.filter(user=request.user)[0]
            address.merchant.save()
            return redirect('manage_address')
        else:
            return render(request, '../templates/UserAddNewAddress.html', {'form': form})


@user_passes_test(is_buyer)
def delete_address(request, address_id):
    address = Address.objects.filter(pk=address_id)
    address.delete()
    return redirect('address_list')


@user_passes_test(is_admin)
def delete_merchant(request, merchant_id):
    merchant = Merchant.objects.filter(pk=merchant_id)[0]
    merchant.delete()
    return redirect('admin_dashboard')


@user_passes_test(is_admin)
def add_category(request):
    if request.method == 'GET':
        form = CategoryForm()
        return render(request, '../templates/AdminAddNewProductType.html', {'form': form})
    else:
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('category_management')
        else:
            return render(request, '../templates/AdminAddNewProductType.html', {'form': form})


@user_passes_test(is_admin)
def delete_category(request, category_id):
    category = Category.objects.filter(pk=category_id)[0]
    category.delete()
    return redirect('category_list')


@user_passes_test(is_admin)
def dashboard_admin(request):
    return render(request, '../templates/AdminDashboard.html')


@user_passes_test(is_seller)
def dashboard_seller(request):
    seller = Seller.objects.filter(user=request.user)[0]
    return render(request, '../templates/StoreDashboard.html', {'seller': seller})


@user_passes_test(is_buyer)
def dashboard_buyer(request):
    return render(request, '../templates/UserDashboard.html')


@user_passes_test(is_admin)
def category_management(request):
    return render(request, '../templates/AdminManageProductTypes.html')


@user_passes_test(is_admin)
def category_list(request):
    categories = Category.objects.all()
    return render(request, '../templates/AdminShowProductTypes.html', {'categories': categories})


@user_passes_test(is_admin)
def seller_list(request):
    sellers = Seller.objects.all()
    return render(request, '../templates/AdminShowStores.html', {'sellers': sellers})


@user_passes_test(is_admin)
def buyer_list(request):
    buyers = Buyer.objects.all()
    return render(request, '../templates/AdminShowUsers.html', {'buyers': buyers})


@user_passes_test(is_admin)
def delete_seller(request, seller_id):
    seller = Seller.objects.filter(pk=seller_id)[0]
    seller.delete()
    return redirect('seller_list')


@user_passes_test(is_admin)
def delete_buyer(request, buyer_id):
    buyer = Buyer.objects.filter(pk=buyer_id)[0]
    buyer.delete()
    return redirect('buyer_list')


@user_passes_test(is_buyer)
def manage_address(request):
    return render(request, '../templates/UserManageAddresses.html')


@user_passes_test(is_buyer)
def address_list(request):
    merchant = Merchant.objects.filter(user=request.user)[0]
    addresses = Address.objects.filter(merchant=merchant)
    return render(request, '../templates/UserShowAddresses.html', {'addresses': addresses})


def add_advertisement(request):
    if is_admin(request.user):
        if request.method == 'GET':
            form = AdminAddAdvertisementForm()
            return render(request, '../templates/AdminAddNewProduct.html', {'form': form})
        else:
            form = AdminAddAdvertisementForm(request.POST, request.FILES, request=request)
            if form.is_valid():
                form.save()
                return redirect('manage_advertisement')
            else:
                return render(request, '../templates/AdminAddNewProduct.html', {'form': form})
    else:
        seller = Seller.objects.filter(user=request.user)[0]
        if request.method == 'GET':
            form = AddAdvertisementForm()
            return render(request, '../templates/StoreAddNewProduct.html', {'form': form, 'seller': seller})
        else:
            form = AddAdvertisementForm(request.POST, request.FILES, request=request)
            if form.is_valid():
                form.save()
                return redirect('manage_advertisement')
            else:
                return render(request, '../templates/StoreAddNewProduct.html', {'form': form, 'seller': seller})


def delete_advertisement(request, advertisement_id):
    advertisement = Advertisement.objects.filter(pk=advertisement_id)[0]
    advertisement.delete()
    return redirect('advertisement_list')


@user_passes_test(is_seller)
def edit_advertisement(request, advertisement_id):
    advertisement = Advertisement.objects.filter(pk=advertisement_id)[0]
    if request.method == 'GET':
        form = AdvertisementEditForm(instance=advertisement, user=request.user)
        return render(request, '../templates/StoreEditProductDetails.html', {'form': form})
    else:
        form = AdvertisementEditForm(request.POST, request.FILES, instance=advertisement, user=request.user)
        if form.is_valid():
            form.save()
            return redirect('advertisement_list')
        else:
            return render(request, '../templates/StoreEditProductDetails.html', {'form': form})


@user_passes_test(is_buyer)
def buyer_edit_profile(request):
    buyer = Buyer.objects.filter(user=request.user)[0]
    if request.method == 'GET':
        form = BuyerEditProfileForm(instance=buyer, user=buyer.user)
        return render(request, '../templates/UserEditProfile.html', {'form': form})
    else:
        form = BuyerEditProfileForm(request.POST, instance=buyer, user=buyer.user)
        if form.is_valid():
            form.save()
            login(request, buyer.user)
            return redirect('dashboard_buyer')
        else:
            return render(request, '../templates/UserEditProfile.html', {'form': form})


@user_passes_test(is_seller)
def seller_edit_profile(request):
    seller = Seller.objects.filter(user=request.user)[0]
    if request.method == 'GET':
        form = SellerEditProfileForm(instance=seller, user=seller.user)
        return render(request, '../templates/StoreEditProfile.html', {'form': form, 'seller': seller})
    else:
        form = SellerEditProfileForm(request.POST, instance=seller, user=seller.user)
        if form.is_valid():
            form.save()
            login(request, seller.user)
            return redirect('dashboard_seller')
        else:
            return render(request, '../templates/StoreEditProfile.html', {'form': form, 'seller': seller})


def manage_advertisement(request):
    if is_admin(request.user):
        return render(request, '../templates/AdminManageProducts.html')
    elif is_seller(request.user):
        seller = Seller.objects.filter(user=request.user)[0]
        return render(request, '../templates/StoreManageProducts.html', {'seller': seller})


def advertisement_list(request):
    if is_admin(request.user):
        advertisements = Advertisement.objects.all()
        return render(request, '../templates/AdminShowProducts.html', {'advertisements': advertisements})
    if is_seller(request.user):
        seller = Seller.objects.filter(user=request.user)[0]
        advertisements = Advertisement.objects.filter(seller__user=request.user)
        return render(request, '../templates/StoreShowProducts.html', {'advertisements': advertisements, 'seller': seller})


def to_persian_number(number):
    devanagari_nums = ('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹')
    number = str(number)
    persian_price = ''.join(devanagari_nums[int(digit)] for digit in number)
    return persian_price


@user_passes_test(is_buyer)
def search(request, category_id=None):
    similar_ads = []
    transactions = Transaction.objects.filter(buyer__user=request.user)
    transaction_categories = []
    for transaction in transactions:
        transaction_categories.append(transaction.advertisement.category)
    if transaction_categories is not None and len(transaction_categories) > 0:
        most_common = mode(transaction_categories)
        similar_ads = list(Advertisement.objects.filter(category=most_common))
        if len(similar_ads) > 6:
            similar_ads = similar_ads[0:5]
    category = None
    if category_id is not None:
        category = Category.objects.filter(pk=category_id)[0]
    categories = get_category_list(None)
    advertisements = Advertisement.objects.all()
    advertisements_filter = []
    for advertisement in advertisements:
        parent_categories = [None]
        current_category = advertisement.category
        while current_category is not None:
            parent_categories.append(current_category)
            current_category = current_category.parent
        if category in parent_categories:
            advertisements_filter.append(advertisement)
    adv_form = AdvanceSearchForm()
    if request.method == 'GET':
        form = SearchForm()
        advertisements_filter = reorder_ads(advertisements_filter)
        return render(request, '../templates/search.html', {'adv_form': adv_form,
                                                            'form': form,
                                                            'categories': categories,
                                                            'similar_ads': similar_ads,
                                                            'advertisements': advertisements_filter,
                                                            'len_advertisements': to_persian_number(
                                                                len(advertisements_filter))})
    else:
        form = SearchForm(request.POST)
        if form.is_valid():
            advertisements_id = []
            for advertisement in advertisements:
                advertisements_id.append(advertisement.id)
            advertisements = Advertisement.objects.filter(id__in=advertisements_id)
            if form.cleaned_data['title'] is not None:
                advertisements = advertisements.filter(title__contains=form.cleaned_data['title']).values_list('id', flat=True)
                notices = Notice.objects.filter(keywords__contains=form.cleaned_data['title']).values_list('advertisement__id', flat=True)
                new_ads = list(advertisements)
                for notice in notices:
                    new_ads.append(notice)
                advertisements = Advertisement.objects.filter(pk__in=new_ads)

            advertisements = reorder_ads(advertisements)
            return render(request, '../templates/search.html', {'form': form,
                                                                'adv_form': adv_form,
                                                                'categories': categories,
                                                                'advertisements': advertisements,
                                                                'similar_ads': similar_ads,
                                                                'len_advertisements': to_persian_number(
                                                                    len(advertisements))})
        else:
            advertisements_filter = reorder_ads(advertisements_filter)
            return render(request, '../templates/search.html', {'form': form,
                                                                'adv_form': adv_form,
                                                                'categories': categories,
                                                                'advertisements': advertisements_filter,
                                                                'similar_ads': similar_ads,
                                                                'len_advertisements': to_persian_number(
                                                                    len(advertisements_filter))})


@user_passes_test(is_buyer)
def advertisement_detail(request, advertisement_id):
    categories = get_category_list(None)
    advertisements = Advertisement.objects.all()
    advertisements_filter = []
    if request.method == 'GET':
        form = SearchForm()
        advertisement = Advertisement.objects.filter(id=advertisement_id)[0]
        campaigns = Campaign.objects.filter(advertisement=advertisement)
        campaigns = campaigns.filter(start_date__range=["2011-01-01", datetime.date.today()],
                                    end_date__range=[datetime.date.today(), "2050-01-01"])
        new_price = to_persian_number(advertisement.price)
        if len(campaigns) > 0:
            new_price = to_persian_number(advertisement.price - advertisement.price * campaigns[0].discount // 100)
        return render(request, '../templates/product.html', {'form': form,
                                                             'advertisement': advertisement,
                                                             'new_price': new_price
                                                             })
    else:
        form = SearchForm(request.POST)
        if form.is_valid():
            advertisements_id = []
            for advertisement in advertisements:
                advertisements_id.append(advertisement.id)
            advertisements = Advertisement.objects.filter(id__in=advertisements_id)
            if form.cleaned_data['title'] is not None:
                advertisements = advertisements.filter(title__contains=form.cleaned_data['title'])
            advertisements = list(advertisements)
            advertisements = reorder_ads(advertisements)
            return render(request, '../templates/search.html', {'adv_form': AdvanceSearchForm(),
                                                                'form': form,
                                                                'categories': categories,
                                                                'advertisements': advertisements,
                                                                'len_advertisements': to_persian_number(
                                                                    len(advertisements))})
        else:
            advertisements_filter = reorder_ads(advertisements_filter)
            return render(request, '../templates/search.html', {'adv_form': AdvanceSearchForm(),
                                                                'form': form,
                                                                'categories': categories,
                                                                'advertisements': advertisements_filter,
                                                                'len_advertisements': to_persian_number(
                                                                    len(advertisements_filter))})


@user_passes_test(is_buyer)
def purchase(request, advertisement_id):
    advertisement = Advertisement.objects.filter(id=advertisement_id)[0]
    addresses = Address.objects.filter(merchant__user=request.user)
    campaigns = Campaign.objects.filter(advertisement=advertisement)
    campaigns = campaigns.filter(start_date__range=["2011-01-01", datetime.date.today()],
                                 end_date__range=[datetime.date.today(), "2050-01-01"])
    new_price = to_persian_number(advertisement.price)
    if len(campaigns) > 0:
        new_price = to_persian_number(advertisement.price - advertisement.price * campaigns[0].discount // 100)
    return render(request, '../templates/cart.html', {'advertisement': advertisement,
                                                      'addresses': addresses,
                                                      'new_price': new_price})


@user_passes_test(is_buyer)
def purchase_done(request, advertisement_id):
    address_id = request.GET.get('address_id')
    time = request.GET.get('time')
    advertisement = Advertisement.objects.filter(id=advertisement_id)[0]
    buyer = Buyer.objects.filter(user=request.user)[0]
    address = Address.objects.filter(id=address_id)[0]
    transaction = Transaction(advertisement=advertisement, buyer=buyer, address=address, time=time)
    transaction.save()
    return render(request, 'Successful_shop.html', {'transaction_id': transaction.id})

@user_passes_test(is_buyer)
def payement(request, transaction_id, done):
    transaction = Transaction.objects.filter(id=transaction_id)[0]
    if done:
        transaction.advertisement.seller.wallet -= transaction.advertisement.category.percentile * transaction.advertisement.price / 100
        transaction.advertisement.seller.save()
        return redirect('search')
    else:
        transaction.delete()
        return redirect('search')


@user_passes_test(is_buyer)
def purchase_list(request):
    buyer = Buyer.objects.filter(user=request.user)[0]
    transactions = Transaction.objects.filter(buyer=buyer)
    return render(request, '../templates/UserShowBoughtProducts.html', {'transactions': transactions})


@user_passes_test(is_admin)
def sell_statistic(request):
    if request.method == 'GET':
        form = SellerForm()
        return render(request, 'AdminStoreSalesInfo.html', {'form': form})
    else:
        form = SellerForm(request.POST)
        if form.is_valid():
            advertisements = Advertisement.objects.filter(seller__company_name=form.cleaned_data['title'])
            advertisements_transaction = []
            for advertisement in advertisements:
                advertisements_transaction.append(
                    (advertisement, to_persian_number(Transaction.objects.filter(advertisement=advertisement).count())))

            return render(request, 'AdminStoreSalesInfo.html', {
                'advertisements': advertisements_transaction,
                'form': form
            })
        else:
            return render(request, 'AdminStoreSalesInfo.html', {'form': form})


@user_passes_test(is_seller)
def store_sales_info(request):
    seller = Seller.objects.filter(user=request.user)[0]
    advertisements = Advertisement.objects.filter(seller__user=request.user)
    advertisements_transaction = []
    for advertisement in advertisements:
        advertisements_transaction.append(
            (advertisement, to_persian_number(Transaction.objects.filter(advertisement=advertisement).count())))
    return render(request, 'StoreShowSalesInfo.html', {
        'advertisements': advertisements_transaction, 'seller': seller,
    })


@user_passes_test(is_seller)
def charge_wallet(request):
    if request.method == 'GET':
        form = WalletForm()
        seller = Seller.objects.filter(user=request.user)[0]
        return render(request, 'StoreChargeWallet.html', {'form': form, 'seller': seller})
    else:
        form = WalletForm(request.POST)
        if form.is_valid():
            seller = Seller.objects.filter(user=request.user)[0]
            seller.wallet += form.cleaned_data['value']
            seller.save()
            return redirect('dashboard_seller')
        else:
            seller = Seller.objects.filter(user=request.user)[0]
            return render(request, 'StoreChargeWallet.html', {'form': form, 'seller': seller})


@user_passes_test(is_buyer)
def advance_search(request):
    categories = get_category_list(None)
    form = AdvanceSearchForm(request.POST)
    if form.is_valid():
        query_params = dict()
        if form.cleaned_data['seller'] is not None:
            query_params['seller'] = form.cleaned_data['seller']
        if form.cleaned_data['color'] is not None and form.cleaned_data['color'] is not '':
            query_params['color'] = form.cleaned_data['color']
        ads = Advertisement.objects.all()
        ads = ads.filter(**query_params)
        minimum_price = 0
        maximum_price = 100000000000
        if form.cleaned_data['minimum_price'] is not None:
            minimum_price = form.cleaned_data['minimum_price']
        if form.cleaned_data['maximum_price'] is not None:
            maximum_price = form.cleaned_data['maximum_price']
        ads = ads.filter(price__range=(minimum_price, maximum_price))
        ads = reorder_ads(ads)
        return render(request, '../templates/search.html', {'adv_form': form,
                                                            'form': SearchForm(),
                                                            'categories': categories,
                                                            'advertisements': ads,
                                                            'len_advertisements': to_persian_number(
                                                                len(ads))
                                                            })
    else:
        ads = Advertisement.objects.all()
        ads = reorder_ads(ads)
        return render(request, '../templates/search.html', {'adv_form': form,
                                                            'form': SearchForm(),
                                                            'categories': categories,
                                                            'advertisements': ads,
                                                            'len_advertisements': to_persian_number(
                                                                len(ads))
                                                            })

@user_passes_test(is_seller)
def manage_campaigns(request):
    seller = Seller.objects.filter(user=request.user)[0]
    return render(request, '../templates/StoreManageCampaigns.html', {'seller': seller})

@user_passes_test(is_seller)
def manage_notices(request):
    seller = Seller.objects.filter(user=request.user)[0]
    return render(request, '../templates/StoreManageAdvertisements.html', {'seller': seller})


@user_passes_test(is_seller)
def add_campaign(request):
    seller = Seller.objects.filter(user=request.user)[0]
    if request.method == 'GET':
        form = AddCampaignForm(user=request.user)
        return render(request, '../templates/StoreAddNewCampaign.html', {'form': form, 'seller': seller})
    else:
        form = AddCampaignForm(request.POST, user=request.user)
        if form.is_valid():
            form.save()
            return redirect('manage_campaigns')
        else:
            return render(request, 'StoreAddNewCampaign.html', {'form': form, 'seller': seller})

@user_passes_test(is_seller)
def add_notice(request):
    seller = Seller.objects.filter(user=request.user)[0]
    if request.method == 'GET':
        form = AddNoticeForm(user=request.user)
        return render(request, '../templates/StoreAddNewAdvertisement.html', {'form': form, 'seller': seller})
    else:
        form = AddNoticeForm(request.POST, user=request.user)
        if form.is_valid():
            form.save()
            return redirect('manage_notices')
        else:
            return render(request, 'StoreAddNewAdvertisement.html', {'form': form, 'seller': seller})

@user_passes_test(is_seller)
def campaign_list(request):
    seller = Seller.objects.filter(user=request.user)[0]
    campaigns = Campaign.objects.filter(advertisement__seller__user=request.user)
    return render(request, '../templates/StoreShowCampaigns.html', {'campaigns': campaigns, 'seller': seller})

@user_passes_test(is_seller)
def notice_list(request):
    seller = Seller.objects.filter(user=request.user)[0]
    notices = Notice.objects.filter(advertisement__seller__user=request.user)
    return render(request, '../templates/StoreShowAdvertisements.html', {'notices': notices, 'seller': seller})