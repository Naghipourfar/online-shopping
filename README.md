[![pipeline status](https://gitlab.com/Naghipourfar/online-shopping/badges/master/pipeline.svg)](https://gitlab.com/Naghipourfar/online-shopping/pipelines)

# Online Shopping

# Installation

This project is based on python 3.7.
Although all python 3 versions are supported, We recommend you to initialize a clean 3.7 python virtual environment and start from it.

After that you can install required python packages with the following command.

	pip install -r requirements.txt
	
	
# Testing

You can use the following command to run unittests and see a report of code coverage.

    coverage run --source='.' manage.py test && coverage report
    
# Running

For running the project you need to run `Django`. Simply run following commands.

    python manage.py runserver

Open `127.0.0.0:8000` in your browser and enjoy!


# Logging

The server will log most of its actions in `./server.log` file.

Note that `./server.log` file size grows with each action.
You may need to apply a log rotation policy.


